const mongoose = require("mongoose");

const serviceSchema = new mongoose.Schema({
  nom: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  price: {
    type: Number,
    required: true,
  },
  quantity : {
    type : Number,
    default : 1
  }
});

const Service = mongoose.model("Service", serviceSchema);

module.exports = Service;
