const mongoose = require("mongoose"); // Erase if already required

// Declare the Schema of the Mongo model
var enquirySchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
    },
    mobile: {
      type: String,
      required: true,
      unique: true,
    },
    comment: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ["success", "pending", "fail"],
    },
  },
  {
    timestamps: true,
  }
);

//Export the model
module.exports = mongoose.model("Enquiry", enquirySchema);
