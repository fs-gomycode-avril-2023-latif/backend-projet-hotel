const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
      lowercase: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

const Category = mongoose.model("Category", categorySchema);

module.exports = Category;
