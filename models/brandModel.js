const mongoose = require("mongoose");

const brandSchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
      lowercase: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

const Brand = mongoose.model("Brand", brandSchema);

module.exports = Brand;
