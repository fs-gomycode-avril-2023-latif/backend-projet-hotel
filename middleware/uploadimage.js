const multer = require("multer");
const sharp = require("sharp");
const path = require("path");
const fs = require('fs')

const multerSorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, "../public/images"));
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.round() * 1e9);
    cb(null, file.fieldname + "-" + uniqueSuffix + ".jpeg");
  },
});

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(
      {
        message: "Unsupported file format",
      },
      false
    );
  }
};

const uploadPhoto = multer({
  storage: multerSorage,
  fileFilter: multerFilter,
  limits: { fieldSize: 5000000 },
});

const roomImageResize = async (req, res, next) => {
  if (!req.files) return next();
  await Promise.all(
    req.files.map(async (file) => {
      await sharp(file.path)
        .resize(300, 300)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`public/images/rooms/${file.filename}`);
        fs.unlinkSync(`public/images/rooms/${file.filename}`)
    })
  );
  next();
};

const userImageResize = async (req, res, next) => {
  if (!req.files) return next();
  await Promise.all(
    req.files.map(async (file) => {
      await sharp(file.path)
        .resize(300, 300)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`public/images/users/${file.filename}`);
        fs.unlinkSync(`public/images/users/${file.filename}`)
    })
  );
  next();
};

module.exports = { uploadPhoto, roomImageResize, userImageResize };
