const jwt = require("jsonwebtoken");
const asyncHandler = require("express-async-handler");
const User = require("../models/userModel");

const authMiddleware = asyncHandler(async (req, res, next) => {
  let token;
  if (req?.headers?.authorization?.startsWith("Bearer")) {
    token = req.headers.authorization.split(" ")[1];
    try {
      if (token) {
        const decoded = jwt.verify(token, process.env.MY_SECRET_PASS_TOKEN );
        const user = await User.findById(decoded?.userId);
        req.user = user;
        next();
      }
    } catch (error) {
      res.status(401).json({ message: `Token invalide ` });
    }
  } else {
    res.status(401).json({ message: `Il ya pas de token ` });
  }
});

const isAdmin = asyncHandler(async (req, res, next) => {
  const { email } = req.user;
  const adminUser = await User.findOne({ email });
  if (adminUser.roles[0] !== "admin") {
    res.status(401).json({message: `Vous etes pas admininstrateur, vous etes ${adminUser.roles[0]} `});
  } else {
    next();
  }
//   console.log(req.user)
});
module.exports = { authMiddleware, isAdmin };
