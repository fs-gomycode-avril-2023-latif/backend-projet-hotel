const express = require("express");
const dbConnect = require("./config/dbconnect");
const app = express();
require("dotenv").config();
const PORT = process.env.PORT;
const bodyParser = require("body-parser");
const authRouter = require("./routes/authRouter");
const roomRouter = require("./routes/roomRouter");
const categoryRouter = require("./routes/categoryRouter");
const serviceRouter = require("./routes/serviceRouter");
const brandRouter = require("./routes/brandRouter");
const couponRouter = require("./routes/couponRouter");
const enquiryRouter = require("./routes/enquiryRouter");
const uploadRouter = require("./routes/uploadRouter");
const cookieParser = require("cookie-parser");
const {
  routeIntrouvable,
  gestionErreur,
} = require("./middleware/GestionnaireErreur");
const morgan = require("morgan");
const cors = require("cors");

// connexion à la base de donnée
dbConnect();

// Utilisation du body-parser pour analyser les données JSON
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

// routes
app.use("/api/user", authRouter);
app.use("/api/room", roomRouter);
app.use("/api/category", categoryRouter);
app.use("/api/service", serviceRouter);
app.use("/api/brand", brandRouter);
app.use("/api/coupon", couponRouter);
app.use("/api/enquiry", enquiryRouter);
app.use("/api/upload", uploadRouter);

// gestion des erreurs de routes
app.use(routeIntrouvable);
app.use(gestionErreur);

// connexion au serveur
app.listen(PORT, () => console.log(`demarrage du serveur sur le port ${PORT}`));
