const jwt = require('jsonwebtoken');

// Fonction pour créer un token JWT
const generateToken = (userId) => {
  const token = jwt.sign({ userId }, process.env.MY_SECRET_PASS_TOKEN, { expiresIn: '1d' }); // Vous pouvez ajuster la durée de validité
  return token;
};

// Fonction pour vérifier un token JWT
// const verifyToken = (token) => {
//   try {
//     const decodedToken = jwt.verify(token, process.env.MY_SECRET_PASS_TOKEN);
//     return decodedToken;
//   } catch (error) {
//     throw new Error('Token invalide');
//   }
// };

module.exports = { generateToken };
