const jwt = require('jsonwebtoken');

// Fonction pour créer un token JWT
const generaterefreshToken = (userId) => {
  const token = jwt.sign({ userId }, process.env.MY_SECRET_PASS_TOKEN, { expiresIn: '2d' }); // Vous pouvez ajuster la durée de validité
  return token;
};

module.exports = { generaterefreshToken };