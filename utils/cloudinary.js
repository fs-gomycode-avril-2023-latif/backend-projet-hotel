const cloudinary = require("cloudinary").v2;

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_PASS,
  api_secret: process.env.CLOUD_SECRET_API,
});

const cloudinaryUploadImg = async (fileToUpload, folder) => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader.upload(
      fileToUpload,
      { resource_type: "auto", folder: folder },
      (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve({
            url: result.secure_url,
            public_id: result.public_id,
            asset_id: result.asset_id,
          });
        }
      }
    );
  });
};

const cloudinaryDeleteImg = async (publicId, folder) => {
  return new Promise((resolve, reject) => {
    const options = {
      resource_type: "image",
      folder: folder,
    };

    cloudinary.uploader.destroy(publicId, options, (error, result) => {
      if (error) {
        reject(error);
      } else {
        resolve(result);
      }
    });
  });
};

module.exports = { cloudinaryUploadImg, cloudinaryDeleteImg };
