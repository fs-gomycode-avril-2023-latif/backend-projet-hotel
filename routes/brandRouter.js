const express = require("express");
const {
  createBrand,
  getaBrand,
  getallBrand,
  updateBrand,
  deleteBrand,
} = require("../controllers/brandCrtl");
const { isAdmin, authMiddleware } = require("../middleware/authMiddleware");
const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createBrand);

Router.get("/all-brand", getallBrand);
Router.get("/:id", authMiddleware, getaBrand);

Router.put("/:id", authMiddleware, isAdmin, updateBrand);

Router.delete("/:id", authMiddleware, isAdmin, deleteBrand);

module.exports = Router;
