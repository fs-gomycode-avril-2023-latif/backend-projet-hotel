const express = require("express");
const {
  createUser,
  loginUser,
  getallUsers,
  getaUser,
  deleteUser,
  updateUser,
  blockUser,
  unlockUser,
  handleRefreshToken,
  logout,
  updatePassword,
  forgotPasswordToken,
  resetPassword,
  uploadImages,
  loginAdmin,
  getWhislist,
  saveAddress,
  userCart,
  getUserCart,
  emptyCart,
  applyCoupon,
  createOrder,
  getOrders,
  updateOrderStatus,
  getallOrders,
} = require("../controllers/userCtrl");
const { authMiddleware, isAdmin } = require("../middleware/authMiddleware");
const { uploadPhoto, userImageResize } = require("../middleware/uploadimage");

const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createUser);
Router.post("/login", loginUser);
Router.post("/admin-login", loginAdmin);
Router.post("/cart", authMiddleware, userCart);
Router.post("/cart/apply-coupon", authMiddleware, applyCoupon);
Router.post("/cart/cash-order", authMiddleware, createOrder);
Router.post("/forgot-password-token", forgotPasswordToken);

Router.put(
  "/upload/:id",
  authMiddleware,
  isAdmin,
  uploadPhoto.array("images", 2),
  userImageResize,
  uploadImages
);

Router.get("/get-orders", authMiddleware, getOrders);
Router.get("/getallorders", authMiddleware,isAdmin, getallOrders);
Router.get("/get-cart", authMiddleware, getUserCart);
Router.get("/whislist", authMiddleware, getWhislist);
Router.get("/logout", logout);
Router.get("/refresh", handleRefreshToken);
Router.get("/all-users", getallUsers);
Router.get("/:id", authMiddleware, isAdmin, getaUser);

Router.delete("/empty-cart", authMiddleware, emptyCart);
Router.delete("/:id", authMiddleware, isAdmin, deleteUser);

Router.put("/save-address", authMiddleware, saveAddress);
Router.put("/reset-password/:token", resetPassword);
Router.put("/password", authMiddleware, updatePassword);
Router.put("/:id", authMiddleware, isAdmin, updateUser);
Router.put("/block-user/:id", authMiddleware, isAdmin, blockUser);
Router.put("/unlock-user/:id", authMiddleware, isAdmin, unlockUser);
Router.put("/order/update-order/:id", authMiddleware,isAdmin, updateOrderStatus);

module.exports = Router;
