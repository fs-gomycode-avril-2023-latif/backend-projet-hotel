const express = require("express");
const {
  createCategory,
  getaCatgory,
  getallCategory,
  updateCategory,
  deleteCategory,
} = require("../controllers/categoryCrtl");
const { isAdmin, authMiddleware } = require("../middleware/authMiddleware");
const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createCategory);

Router.get("/all-category", getallCategory);
Router.get("/:id", authMiddleware, getaCatgory);

Router.put("/:id", authMiddleware, isAdmin, updateCategory);

Router.delete("/:id", authMiddleware, isAdmin, deleteCategory);

module.exports = Router;
