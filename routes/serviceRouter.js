const express = require("express");
const {
  createService,
  getallService,
  getaService,
  updateService,
  deleteService,
} = require("../controllers/serviceCrtl");
const { authMiddleware, isAdmin } = require("../middleware/authMiddleware");
const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createService);

Router.get("/all-service", getallService);
Router.get("/:id", authMiddleware, getaService);

Router.put("/:id", authMiddleware, isAdmin, updateService);

Router.delete("/:id", authMiddleware, isAdmin, deleteService);

module.exports = Router;
