const express = require("express");
const { isAdmin, authMiddleware } = require("../middleware/authMiddleware");
const { createEnquiry, getallEnquiry, getaEnquiry, updateEnquiry, deleteEnquiry } = require("../controllers/enquiryCrtl");
const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createEnquiry);

Router.get("/all-enquiry", getallEnquiry);
Router.get("/:id", authMiddleware, getaEnquiry);

Router.put("/:id", authMiddleware, isAdmin, updateEnquiry);

Router.delete("/:id", authMiddleware, isAdmin, deleteEnquiry);

module.exports = Router;
