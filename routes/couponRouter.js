const express = require("express");
const {
  createCoupon,
  getallCoupon,
  getaCoupon,
  deleteCoupon,
  updateCoupon,
} = require("../controllers/couponCrtl");
const { authMiddleware, isAdmin } = require("../middleware/authMiddleware");
const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createCoupon);

Router.get("/all-coupon", authMiddleware, isAdmin, getallCoupon);
Router.get("/:id", authMiddleware, isAdmin, getaCoupon);

Router.put("/:id", authMiddleware, isAdmin, updateCoupon);

Router.delete("/:id", authMiddleware, isAdmin, deleteCoupon);

module.exports = Router;
