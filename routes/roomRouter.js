const express = require("express");
const {
  createRoom,
  getallRoom,
  getaRoom,
  updateRoom,
  deleteRoom,
  addToWishlist,
  rating,
} = require("../controllers/roomCrtl");
const { isAdmin, authMiddleware } = require("../middleware/authMiddleware");
const Router = express.Router();

Router.post("/register", authMiddleware, isAdmin, createRoom);

Router.get("/all-room", getallRoom);
Router.get("/:id", authMiddleware, getaRoom);

Router.put("/wishlist", authMiddleware, addToWishlist);
Router.put("/rating", authMiddleware, rating);
Router.put("/:id", authMiddleware, isAdmin, updateRoom);

Router.delete("/:id", authMiddleware, isAdmin, deleteRoom);

module.exports = Router;
