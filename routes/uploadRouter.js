const express = require("express");
const { uploadImages, deleteImage } = require("../controllers/uploadCrtl");
const { isAdmin, authMiddleware } = require("../middleware/authMiddleware");
const { uploadPhoto, roomImageResize } = require("../middleware/uploadimage");
const router = express.Router();

router.post(
  "/",
  authMiddleware,
  isAdmin,
  uploadPhoto.array("images", 10),
  roomImageResize,
  uploadImages
);

router.delete("/:id", authMiddleware, isAdmin, deleteImage);
module.exports = router;
