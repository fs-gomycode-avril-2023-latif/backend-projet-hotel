const asyncHandler = require("express-async-handler");
const { validatemongoDbId } = require("../utils/validateMongoDBid");
const fs = require("fs");
const {
  cloudinaryUploadImg,
  cloudinaryDeleteImg,
} = require("../utils/cloudinary");

const uploadImages = asyncHandler(async (req, res) => {
  try {
    const uploader = (path) => cloudinaryUploadImg(path, "images");
    const uploadedImages = []; // Un tableau pour stocker les informations des images téléchargées

    const files = req.files;
    for (const file of files) {
      const { path } = file;
      const newPath = await uploader(path);

      // Ajouter le public_id et asset_id aux informations de l'image téléchargée
      const public_id = newPath.public_id;
      const asset_id = newPath.asset_id;

      // Ajouter les informations de l'image au tableau
      uploadedImages.push({
        url: newPath.url,
        public_id: public_id,
        asset_id: asset_id,
      });

      fs.unlinkSync(path);
    }

    res.json(uploadedImages); // Renvoyer les informations de toutes les images téléchargées
  } catch (error) {
    throw new Error(`error image ${error}`);
  }
});

const deleteImage = asyncHandler(async (req, res) => {
  try {
    const publicId = req.params;
    const folder = "images"; // Remplacez par le dossier correct dans Cloudinary
    const deletionResult = await cloudinaryDeleteImg(publicId, folder);
    res
      .status(200)
      .json({ message: "Image supprimée avec succès", result: deletionResult });
  } catch (error) {
    res.status(500).json({
      message: "Une erreur est survenue lors de la suppression de l'image",
      error: error.message,
    });
  }
});

module.exports = { uploadImages, deleteImage };
