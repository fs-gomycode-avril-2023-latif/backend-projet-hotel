const asyncHandler = require("express-async-handler");
const Brand = require("../models/brandModel");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const createBrand = asyncHandler(async (req, res) => {
  try {
    const newRoom = await Brand.create(req.body);
    res.json(newRoom);
  } catch (error) {
    throw new Error(error);
  }
});

const getaBrand = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const brand = await Brand.findById(id);
    res.json(brand);
  } catch (error) {
    console.log(error);
  }
});

const getallBrand = asyncHandler(async (req, res) => {
  try {
    const allBrand = await Brand.find();
    res.json(allBrand);
  } catch (error) {
    throw new Error(`error all brand : ${error}`);
  }
});

const updateBrand = asyncHandler(async (req, res) => {
  const id = req.params;

  try {
    if (id !== undefined) {
      const objectId = new ObjectId(req.params.id); //

      const updatedBrand = await Brand.findByIdAndUpdate(objectId, req.body, {
        new: true,
      });
      res.json(updatedBrand);
    } else {
      throw new Error("No brand number provided.");
    }
  } catch (error) {
    throw new Error(`Error updating brand: ${error}`);
  }
});

const deleteBrand = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const deletedBrand = await Brand.findByIdAndDelete(id);
    res.json({ deletedBrand });
  } catch (error) {
    throw new Error(`error delete Brand : ${error}`);
  }
});

module.exports = {
  createBrand,
  getaBrand,
  updateBrand,
  getallBrand,
  deleteBrand,
};
