const { generateToken } = require("../config/jsonwebtoken");
const User = require("../models/userModel");
const Room = require("../models/roomModel");
const Cart = require("../models/cartModel");
const Coupon = require("../models/couponModel");
const Order = require("../models/orderModel");
const asyncHandler = require("express-async-handler");
const { validatemongoDbId } = require("../utils/validateMongoDBid");
const { generaterefreshToken } = require("../config/jwtrefreshtoken");
const jwt = require("jsonwebtoken");
const { sendEmail } = require("./sendEmail");
const crypto = require("crypto");
const cloudinaryUploadImg = require("../utils/cloudinary");
const fs = require("fs");
const uniqid = require("uniqid");

// ajouter un utilisateur
const createUser = asyncHandler(async (req, res) => {
  const email = req.body.email;
  const findUser = await User.findOne({ email: email });

  if (!findUser) {
    try {
      // Créer un nouvel utilisateur et attendre que l'opération se termine
      const newUser = await User.create(req.body);
      res.json(newUser);
    } catch (error) {
      // throw new Error(error);
      // Gérer les erreurs liées à la création de l'utilisateur
      res.status(500).json({
        message: `Erreur lors de la création de l'utilisateur : ${error}`,
      });
    }
  } else {
    // L'utilisateur existe déjà
    res.status(400).json({ message: "L'utilisateur existe déjà" });
  }
});

// login user
const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const findUser = await User.findOne({ email });
  if (findUser && (await findUser.isPasswordMatched(password))) {
    const token = generateToken(findUser?._id);
    const refreshToken = await generaterefreshToken(findUser?._id);
    const updatetoken = await User.findByIdAndUpdate(
      findUser?._id,
      {
        refreshToken: refreshToken,
      },
      {
        new: true,
      }
    );
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });
    res.json({
      _id: findUser?._id,
      nom: findUser?.nom,
      prenom: findUser?.prenom,
      email: findUser?.email,
      tel: findUser?.tel,
      role: findUser?.role,
      poste: findUser?.poste,
      token: token,
    });
  } else {
    // throw new Error("Informations d’identification non valides");

    res
      .status(500)
      .json({ message: "Informations d’identification non valides" });
  }
});

// login admin
const loginAdmin = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const findAdmin = await User.findOne({ email });
  if (findAdmin.roles[0] !== "admin")
    throw new Error("Vous etes pas autorise a vous connecter ici");
  if (findAdmin && (await findAdmin.isPasswordMatched(password))) {
    const token = generateToken(findAdmin?._id);
    const refreshToken = await generaterefreshToken(findAdmin?._id);
    const updatetoken = await User.findByIdAndUpdate(
      findAdmin?._id,
      {
        refreshToken: refreshToken,
      },
      {
        new: true,
      }
    );
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });
    res.json({
      _id: findAdmin?._id,
      nom: findAdmin?.nom,
      prenom: findAdmin?.prenom,
      email: findAdmin?.email,
      tel: findAdmin?.tel,
      role: findAdmin?.role,
      poste: findAdmin?.poste,
      token: token,
    });
  } else {
    // throw new Error("Informations d’identification non valides");

    res
      .status(500)
      .json({ message: "Informations d’identification non valides" });
  }
});

// handle refresh cookie
const handleRefreshToken = asyncHandler(async (req, res) => {
  const cookie = req.cookies;
  if (!cookie?.refreshToken) throw new Error("No refresh Token in cookies ");
  const refreshToken = cookie.refreshToken;
  // recupere le user en fonction du token
  const user = await User.findOne({ refreshToken });
  if (!user) throw new Error("No refresh token present in db or not matched");
  jwt.verify(refreshToken, process.env.MY_SECRET_PASS_TOKEN, (err, decoded) => {
    if (err || user.id !== decoded.userId) {
      throw new Error("there is something wrong with refresh token");
    }
    const accessToken = generateToken(user?._id);
    res.json({ accessToken });
  });
});

// fonction de deconnexion

const logout = asyncHandler(async (req, res) => {
  const cookie = req.cookies;
  if (!cookie?.refreshToken) throw new error("No refresh Token in cookies ");
  const refreshToken = cookie.refreshToken;
  const user = await User.findOne({ refreshToken });
  if (!user) {
    res.clearCookie("refreshToken", {
      httpOnly: true,
      secure: true,
    });
    return res.sendStatus(204); //forbiden
  }
  await User.findOneAndUpdate(
    { refreshToken },
    {
      refreshToken: "",
    }
  );
  res.clearCookie("refreshToken", {
    httpOnly: true,
    secure: true,
  });
  res.sendStatus(204); //forbiden
});

// liste des utilisateurs
const getallUsers = asyncHandler(async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (error) {
    // throw new Error(error);
    res.status(500).json({
      message: `Erreur lors de la recuperation de tous les utilisateurs ${error}`,
    });
  }
});

// Recuperer un utilisateur
const getaUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const user = await User.findById(id);
    res.json({ user });
  } catch (error) {
    // throw new Error(`error single user : ${error}`);

    res.status(500).json({
      message: `Erreur lors de la recuperation d'un utilisateurs ${error}`,
    });
  }
});

// supprimer un utilisateur
const deleteUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const deleteUser = await User.findByIdAndDelete(id);
    res.json({ deleteUser });
  } catch (error) {
    // throw new Error(`error delete user : ${error}`);
    res.status(500).json({
      message: `Erreur lors de la recuperation d'un utilisateurs ${error}`,
    });
  }
});

// modifier un utilisateur
const updateUser = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    const updatedUser = await User.findByIdAndUpdate(
      _id,
      {
        nom: req?.body?.nom,
        prenom: req?.body?.prenom,
        email: req?.body?.email,
        tel: req?.body?.tel,
        roles: req?.body?.roles,
        poste: req?.body?.poste,
      },
      {
        new: true,
      }
    );
    res.json(updatedUser);
  } catch (error) {
    res.status(500).json({
      message: `Erreur lors de la mise à jour de l'utilisateur : ${error.message}`,
    });
  }
});

// save user address
const saveAddress = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    const updatedUser = await User.findByIdAndUpdate(
      _id,
      {
        address: req?.body?.address,
      },
      {
        new: true,
      }
    );
    res.json(updatedUser);
  } catch (error) {
    res.status(500).json({
      message: `Erreur lors de la mise à jour de l'utilisateur : ${error.message}`,
    });
  }
});

const blockUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const blockU = await User.findByIdAndUpdate(
      id,
      {
        isBloqued: true,
      },
      {
        new: true,
      }
    );
    res.json({ message: "Utilisateur bloque" });
  } catch (error) {
    res.status(500).json({
      message: `Erreur lors du blocage de l'utilisateur : ${error.message}`,
    });
  }
});
const unlockUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const unlockU = await User.findByIdAndUpdate(
      id,
      {
        isBloqued: false,
      },
      {
        new: true,
      }
    );
    res.json({ message: "Utilisateur debloque" });
  } catch (error) {
    res.status(500).json({
      message: `Erreur lors du deblocage de l'utilisateur : ${error.message}`,
    });
  }
});

const updatePassword = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const newPassword = req.body.password; // Assurez-vous d'obtenir correctement le mot de passe du corps de la requête

  validatemongoDbId(_id);

  try {
    const user = await User.findById(_id);
    if (newPassword) {
      user.password = newPassword;
      const updatedUser = await user.save(); // Utilisez "user.save()" pour enregistrer l'instance de l'utilisateur

      console.log("User saved:", updatedUser);
      res.json(updatedUser);
    } else {
      res.json(user);
    }
  } catch (error) {
    console.error("Error updating password:", error);
    res.status(500).json({ error: `An error occurred: ${error.message}` });
  }
});

// mot de passe oublie => envoi de mail

const forgotPasswordToken = asyncHandler(async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });
  if (!user) throw new Error("utilisateur existe pas avec identifant email");
  try {
    const token = await user.createResetPasswordToken();
    await user.save();
    const resetURL = `Please folow this link to reset your password , this link is valid till 10 minutes <a href='http ://localhost:3030/api/user/reset-password/${token}'>click here</a>`;
    const data = {
      to: email,
      subject: "reset forgot password",
      text: `hey ! ${user.nom} ${user.prenom}`,
      htm: resetURL,
    };
    sendEmail(data);
    res.json(token);
  } catch (error) {
    throw new Error(error);
  }
});

const resetPassword = asyncHandler(async (req, res) => {
  const { password } = req.body;
  const { token } = req.params;
  const hashedToken = crypto.createHash("sha256").update(token).digest("hex");
  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: { $gt: Date.now() },
  });
  if (!user) throw new Error("Token expired , Please try again later");
  user.password = password;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();
  res.json(user);
});

const uploadImages = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);

  try {
    const uploader = async (path) => {
      const result = await cloudinaryUploadImg(path);
      return result.url; // Assurez-vous que ceci correspond à l'emplacement de l'URL dans la réponse
    };

    const urls = [];
    const files = req.files;

    for (const file of files) {
      const { path } = file;
      const newPath = await uploader(path);
      urls.push(newPath);
      fs.unlinkSync(path);
    }

    const updatedUser = await User.findByIdAndUpdate(
      id,
      {
        images: urls,
      },
      {
        new: true,
      }
    );

    res.json(updatedUser);
  } catch (error) {
    throw new Error(`Error uploading images: ${error}`);
  }
});

const getWhislist = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  try {
    const findUser = await User.findById(_id).populate("whishlist");
    res.json(findUser);
  } catch (error) {
    throw new Error(error);
  }
});

const userCart = asyncHandler(async (req, res) => {
  let rooms = [];
  const { cart } = req.body;
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    const user = await User.findById(_id);
    const alreadyExistCart = await Cart.findOne({ orderby: user._id });
    if (alreadyExistCart) {
      alreadyExistCart.remove();
    }
    for (let i = 0; i < cart.length; i++) {
      let object = {};
      object.room = cart[i]._id;
      object.count = cart[i].count;
      object.color = cart[i].color;
      let getPrice = await Room.findById(cart[i]._id).select("price").exec();
      object.price = getPrice.price;
      object.total = getPrice.price * cart[i].count;
      rooms.push(object);
    }
    // console.log(rooms);
    let cartTotal = 0;
    for (let i = 0; i < rooms.length; i++) {
      cartTotal += rooms[i].price * rooms[i].count;
    }
    let newCart = await new Cart({
      rooms,
      cartTotal,
      orderBy: user?._id,
    }).save();
    res.json(newCart);
  } catch (error) {
    throw new Error(`errerur cart ${error}`);
  }
});

const getUserCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    const cart = await Cart.findOne({ orderBy: _id }).populate("rooms.room");
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

const emptyCart = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    const user = await User.findOne({ _id });
    const cart = await Cart.findOneAndRemove({ orderBy: user._id });
    res.json(cart);
  } catch (error) {
    throw new Error(error);
  }
});

const applyCoupon = asyncHandler(async (req, res) => {
  const { coupon } = req.body;
  const { _id } = req.user;
  validatemongoDbId(_id);
  const validCoupon = await Coupon.findOne({ nom: coupon });
  if (validCoupon == null) throw new Error("Invalid coupon");
  const user = await User.findOne({ _id });
  let { cartTotal } = await Cart.findOne({ orderBy: user._id }).populate(
    "rooms.room"
  );
  let totalAfterDiscount = (
    cartTotal -
    (cartTotal - validCoupon.discount) / 100
  ).toFixed(2);
  const newCartafterDiscount = await Cart.findOneAndUpdate(
    { orderBy: user._id },
    {
      totalAfterDiscount,
    },
    { new: true }
  );
  res.json(totalAfterDiscount);
});

const createOrder = asyncHandler(async (req, res) => {
  const { COD, coupon } = req.body;
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    if (!COD) throw new Error("create cash order failed");
    const user = await User.findOne({ _id });
    const userCart = await Cart.findOne({ orderBy: user._id });
    let finalAmount = 0;
    if (coupon && userCart.totalAfterDiscount) {
      finalAmount = userCart.totalAfterDiscount;
    } else {
      finalAmount = userCart.cartTotal;
    }
    let newOrder = await new Order({
      rooms: userCart.rooms,
      paymentInternet: {
        id: uniqid(),
        method: "COD",
        amount: finalAmount,
        status: "Cash on Delivery",
        created: Date.now(),
        currency: "cfa",
      },
      orderBy: user._id,
      orderStatus: "Cash on Delivery",
    }).save();
    console.log(`new orders = ${newOrder}`)
    let update = userCart.rooms.map((item) => {
      return {
        updateOne: {
          filter: { _id: item.room._id },
          update: { $inc: { quantity: -item.count, sold: +item.count } },
        },
      };
    });
    const updated = await Room.bulkWrite(update, {});
    res.json({ message: "success" });
  } catch (error) {
    throw new Error(error);
  }
});

const getOrders = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  validatemongoDbId(_id);
  try {
    const userOrders = await Order.findOne({ orderBy: _id })
      .populate("rooms.room")
      .populate("orderBy")
      .exec();
    res.json(userOrders);
  } catch (error) {
    throw new Error(error);
  }
});

const getallOrders = asyncHandler(async (req, res) => {
  try {
    const allUserOrders = await Order.find()
      .populate("rooms.room")
      .populate("orderBy")
      .exec();
    res.json(allUserOrders);
  } catch (error) {
    throw new Error(error);
  }
});

const updateOrderStatus = asyncHandler(async (req, res) => {
  const { status } = req.body;
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const findOrder = await Order.findByIdAndUpdate(
      id,
      {
        orderStatus: status,
        paymentInternet: {
          status: status,
        },
      },
      { new: true }
    );
    res.json(findOrder);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createUser,
  loginUser,
  getallUsers,
  getaUser,
  deleteUser,
  updateUser,
  blockUser,
  unlockUser,
  handleRefreshToken,
  logout,
  updatePassword,
  forgotPasswordToken,
  resetPassword,
  uploadImages,
  loginAdmin,
  getWhislist,
  saveAddress,
  userCart,
  getUserCart,
  emptyCart,
  applyCoupon,
  createOrder,
  getOrders,
  updateOrderStatus,
  getallOrders
};
