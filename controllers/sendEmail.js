const nodemailer = require("nodemailer");
const asyncHandler = require("express-async-handler");

const sendEmail = asyncHandler(async (data, req, res) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
      user: process.env.MON_EMAIL,
      pass: process.env.MDP, // Si l'authentification à deux facteurs est activée
    },
  });

  const mailOptions = {
    from: process.env.MON_EMAIL,
    to: data.to,
    subject: data.subject,
    text: data.text,
    html: data.htm,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error("Error sending email:", error);
    } else {
      console.log("Email sent:", info.response);
    }
  });
});

module.exports = { sendEmail };
