const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");
const Service = require("../models/serviceModel");
const ObjectId = mongoose.Types.ObjectId;

const createService = asyncHandler(async (req, res) => {
  try {
    const newService = await Service.create(req.body);
    res.json(newService);
  } catch (error) {
    throw new Error(error);
  }
});

const getaService = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const service = await Service.findById(id);
    res.json(service);
  } catch (error) {
    console.log(error);
  }
});

const getallService = asyncHandler(async (req, res) => {
  try {
    const allService = await Service.find();
    res.json(allService);
  } catch (error) {
    throw new Error(`error all category : ${error}`);
  }
});

const updateService = asyncHandler(async (req, res) => {
  const id = req.params;

  try {
    if (id !== undefined) {
      const objectId = new ObjectId(req.params.id); //

      const updatedService = await Service.findByIdAndUpdate(
        objectId,
        req.body,
        {
          new: true,
        }
      );
      res.json(updatedService);
    } else {
      throw new Error("No category number provided.");
    }
  } catch (error) {
    throw new Error(`Error updating category: ${error}`);
  }
});

const deleteService = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const deletedService= await Service.findByIdAndDelete(id);
    res.json({ deletedService });
  } catch (error) {
    throw new Error(`error delete Room : ${error}`);
  }
});

module.exports = {
  createService,
  getaService,
  updateService,
  getallService,
  deleteService,
};
