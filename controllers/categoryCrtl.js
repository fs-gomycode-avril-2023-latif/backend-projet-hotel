const asyncHandler = require("express-async-handler");
const Category = require("../models/categoryModel");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const createCategory = asyncHandler(async (req, res) => {
  try {
    const newRoom = await Category.create(req.body);
    res.json(newRoom);
  } catch (error) {
    throw new Error(error);
  }
});

const getaCatgory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const category = await Category.findById(id);
    res.json(category);
  } catch (error) {
    console.log(error);
  }
});

const getallCategory = asyncHandler(async (req, res) => {
  try {
    const allCategory = await Category.find();
    res.json(allCategory);
  } catch (error) {
    throw new Error(`error all category : ${error}`);
  }
});

const updateCategory = asyncHandler(async (req, res) => {
  const id = req.params;

  try {
    if (id !== undefined) {
      const objectId = new ObjectId(req.params.id); //

      const updatedCategory = await Category.findByIdAndUpdate(
        objectId,
        req.body,
        {
          new: true,
        }
      );
      res.json(updatedCategory);
    } else {
      throw new Error("No category number provided.");
    }
  } catch (error) {
    throw new Error(`Error updating category: ${error}`);
  }
});

const deleteCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const deletedCategory = await Category.findByIdAndDelete(id);
    res.json({ deletedCategory });
  } catch (error) {
    throw new Error(`error delete Room : ${error}`);
  }
});

module.exports = {
  createCategory,
  getaCatgory,
  updateCategory,
  getallCategory,
  deleteCategory,
};
