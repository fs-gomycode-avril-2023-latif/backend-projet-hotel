const asyncHandler = require("express-async-handler");
const Coupon = require("../models/couponModel");
const { validatemongoDbId } = require("../utils/validateMongoDBid");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const createCoupon = asyncHandler(async (req, res) => {
  try {
    const newCoupon = await Coupon.create(req.body);
    res.json(newCoupon);
  } catch (error) {
    throw new Error(error);
  }
});

const getaCoupon = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const coupon = await Coupon.findById(id);
    res.json(coupon);
  } catch (error) {
    console.log(error);
  }
});

const getallCoupon = asyncHandler(async (req, res) => {
  try {
    const allCoupon = await Coupon.find();
    res.json(allCoupon);
  } catch (error) {
    throw new Error(`error all category : ${error}`);
  }
});

const updateCoupon= asyncHandler(async (req, res) => {
  const id = req.params;
 

  try {
    if (id !== undefined) {
      const objectId = new ObjectId(req.params.id); //

      const updatedCoupon= await Coupon.findByIdAndUpdate(
        objectId,
        req.body,
        {
          new: true,
        }
      );
      res.json(updatedCoupon);
    } else {
      throw new Error("No category number provided.");
    }
  } catch (error) {
    throw new Error(`Error updating category: ${error}`);
  }
});

const deleteCoupon = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const deletedCoupon = await Coupon.findByIdAndDelete(id);
    res.json({ deletedCoupon });
  } catch (error) {
    throw new Error(`error delete Room : ${error}`);
  }
});

module.exports = { createCoupon, getaCoupon, getallCoupon, deleteCoupon,updateCoupon };
