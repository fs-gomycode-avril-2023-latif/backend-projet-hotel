const nodemailer = require("nodemailer");

const sendEmail = async (data) => {
  console.log('sendEmail');
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: process.env.MAIL_ID,
      pass: process.env.PASSWORD_EMAIL,
    },
  });

  const info = await transporter.sendMail({
    from: '"HEY! 👻" <foo@gmail.com>',
    to: data.to,
    subject: data.subject,
    text: data.text,
    html: data.html,
  });

  console.log("Message sent: %s", info.messageId);
};

module.exports = sendEmail;
