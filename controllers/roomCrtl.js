const Room = require("../models/roomModel");
const asyncHandler = require("express-async-handler");
const slugify = require("slugify");
const mongoose = require("mongoose");
const User = require("../models/userModel");
const { validatemongoDbId } = require("../utils/validateMongoDBid");
const ObjectId = mongoose.Types.ObjectId;

const createRoom = asyncHandler(async (req, res) => {
  try {
    if (req.body.nochambre) {
      req.body.slug = slugify(req.body.nochambre);
    }
    const newRoom = await Room.create(req.body);
    res.json(newRoom);
  } catch (error) {
    throw new Error(error);
  }
});

const getaRoom = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const room = await Room.findById(id);
    res.json(room);
  } catch (error) {
    console.log(error);
  }
});

const getallRoom = asyncHandler(async (req, res) => {
  try {
    // filttering
    const queryObj = { ...req.query };
    const excludeFields = ["page", "sort", "limit", "fields"];
    excludeFields.forEach((elt) => delete queryObj[elt]);
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    let query = Room.find(JSON.parse(queryStr));

    // Sorting
    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      query = query.sort(sortBy);
    } else {
      query = query.sort("-createdAt");
    }

    // limit
    if (req.query.fields) {
      const fields = req.query.fields.split(",").join(" ");
      query = query.select(fields);
    } else {
      query = query.select("-__v");
    }

    // pagination
    const page = req.query.page;
    const limit = req.query.limit;
    const skip = (page - 1) * limit;
    query = query.skip(skip).limit(limit);
    if (req.query.page) {
      const roomCount = await Room.countDocuments;
      if (skip >= roomCount) throw new Error("la page existe pas");
    }

    const room = await query;
    res.json(room);
  } catch (error) {
    throw new Error(`error all room : ${error}`);
  }
});

// modifier un utilisateur
const updateRoom = asyncHandler(async (req, res) => {
  const id = req.params;
  validatemongoDbId(id);
  if (req.body.nochambre) {
    req.body.slug = slugify(req.body.nochambre);
  }

  try {
    if (id !== undefined) {
      const objectId = new ObjectId(req.params.id); //

      const updatedRoom = await Room.findByIdAndUpdate(objectId, req.body, {
        new: true,
      });
      res.json(updatedRoom);
    } else {
      throw new Error("No room number provided.");
    }
  } catch (error) {
    throw new Error(`Error updating room: ${error}`);
  }
});

const deleteRoom = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validatemongoDbId(id);
  try {
    const deletedRoom = await Room.findByIdAndDelete(id);
    res.json({ deletedRoom });
  } catch (error) {
    throw new Error(`error delete Room : ${error}`);
  }
});

const addToWishlist = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { prodId } = req.body; //id de la chambre
  try {
    const user = await User.findById(_id); //utilisateur connecté
    const alreadyadded = user.whishlist.find((id) => id.toString() == prodId);
    if (alreadyadded) {
      let user = await User.findByIdAndUpdate(
        _id,
        {
          $pull: { whishlist: prodId },
        },
        {
          new: true,
        }
      );
      res.json(user);
    } else {
      let user = await User.findByIdAndUpdate(
        _id,
        {
          $push: { whishlist: prodId },
        },
        {
          new: true,
        }
      );
      res.json(user);
    }
  } catch (error) {
    throw new Error(error);
  }
});

const rating = asyncHandler(async (req, res) => {
  const { _id } = req.user;
  const { star, prodId, comment } = req.body;

  try {
    const room = await Room.findById(prodId);

    // Trouver le rating déjà existant pour cet utilisateur
    const alreadyRated = room.ratings.find(
      (rating) => rating.postedby.toString() === _id.toString()
    );

    if (alreadyRated) {
      // Mettre à jour la note existante
      alreadyRated.star = star;
      alreadyRated.comment = comment;
    } else {
      // Ajouter un nouveau rating
      room.ratings.push({
        star: star,
        postedby: _id,
        comment: comment,
      });
    }

    // Sauvegarder les modifications dans la chambre
    await room.save();

    // Recalculer la note moyenne
    const totalRating = room.ratings.length;
    const ratingsum = room.ratings
      .map((item) => item.star)
      .reduce((prev, curr) => prev + curr, 0);
    const actualRating = Math.round(ratingsum / totalRating);
    // Mise à jour de la note moyenne dans la chambre
    const finalRoom = await Room.findByIdAndUpdate(
      prodId,
      {
        totalrating: actualRating,
      },
      {
        new: true,
      }
    );

    res.json(finalRoom);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createRoom,
  getaRoom,
  getallRoom,
  updateRoom,
  deleteRoom,
  addToWishlist,
  rating,
};
