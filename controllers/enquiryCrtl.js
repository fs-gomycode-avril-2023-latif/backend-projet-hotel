const asyncHandler = require("express-async-handler");
const Enquiry = require("../models/enquiryModel");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const createEnquiry = asyncHandler(async (req, res) => {
  try {
    const newEnq = await Enquiry.create(req.body);
    res.json(newEnq);
  } catch (error) {
    throw new Error(error);
  }
});

const getaEnquiry = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const enq = await Enquiry.findById(id);
    res.json(enq);
  } catch (error) {
    console.log(error);
  }
});

const getallEnquiry = asyncHandler(async (req, res) => {
  try {
    const allEnq = await Enquiry.find();
    res.json(allEnq);
  } catch (error) {
    throw new Error(`error all enquiry : ${error}`);
  }
});

const updateEnquiry = asyncHandler(async (req, res) => {
  const id = req.params;

  try {
    if (id !== undefined) {
      const objectId = new ObjectId(req.params.id); //

      const updatedEnq = await Enquiry.findByIdAndUpdate(
        objectId,
        req.body,
        {
          new: true,
        }
      );
      res.json(updatedEnq);
    } else {
      throw new Error("No enquiry number provided.");
    }
  } catch (error) {
    throw new Error(`Error updating enquiry: ${error}`);
  }
});

const deleteEnquiry = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    const deletedEnq = await Enquiry.findByIdAndDelete(id);
    res.json({ deletedEnq });
  } catch (error) {
    throw new Error(`error delete Room : ${error}`);
  }
});

module.exports = {
  createEnquiry,
  getaEnquiry,
  updateEnquiry,
  getallEnquiry,
  deleteEnquiry,
};
